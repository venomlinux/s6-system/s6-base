# s6-base
A collection of essential s6-rc oneshots and longruns for startup/shutdown. This is based on the works of Artix Linux (http://www.artixlinux.org/), Skarnet (https://skarnet.org), Kiss Linux (https://codeberg.org/git-bruh/s6-scripts), Musl-LFS-S6-Bootscripts (https://github.com/dslm4515/MLFS-S6-Bootscripts) and ideas from Telegram users. Thanks for making this possible.
