SYSCONFDIR ?= /etc
S6DIR = $(SYSCONFDIR)/s6
SVDIR = $(S6DIR)/sv
ADMINSVDIR = $(S6DIR)/adminsv

PREFIX = /usr

LOGRELOAD = log-service-reload.sh
MODLOAD = modules-load
RC = rc.local

CONFIGDIR = $(S6DIR)/config

CONFIG = \
	convfile \
	dmesg.conf \
	hwclock.conf \
	mount-tmpfs.conf \
	network-detection.conf \
	tty1.conf \
	tty2.conf \
	tty3.conf \
	tty4.conf \
	tty5.conf \
	tty6.conf \
	ttyS.conf \
	udevd.conf

BOOTDIR = boot
CLEANUPDIR = cleanup
CSDIR = console-setup
DEFAULTDIR = default
DMESGLOGDIR = dmesg-log
DMESGSRVDIR = dmesg-srv
GETTYDIR = getty
HOSTNAMEDIR = hostname
HWCLOCKDIR = hwclock
LOCALEDIR = locale
MISCDIR = misc
MODULESDIR = modules
MOUNTDIR = mount
MCGDIR = mount-cgroups
MDEVFSDIR = mount-devfs
MFSDIR = mount-filesystems
MNETDIR = mount-net
MPROCDIR = mount-procfs
MSYSFSDIR = mount-sysfs
MTMPFSDIR = mount-tmpfs
NETLODIR = net-lo
NETWODIR = network
NETWODETECTDIR = network-detection
RNDSEEDDIR = random-seed
RCLOCALDIR = rc-local
REMNTROOTDIR = remount-root
SETUPDIR = setup
SWAPDIR = swap
SYSCTLDIR = sysctl
TTY1DIR = tty1
TTY2DIR = tty2
TTY3DIR = tty3
TTY4DIR = tty4
TTY5DIR = tty5
TTY6DIR = tty6
TTYSDIR = ttyS
UDEVDIR = udev
UDEVADMDIR = udevadm
UDEVLOGDIR = udevd-log
UDEVSRVDIR = udevd-srv

BOOT = $(subst boot/contents.d,, $(wildcard boot/*))
BOOTCONTENTS = $(wildcard boot/*/*)
CLEANUP = $(subst cleanup/dependencies.d,, $(wildcard cleanup/*))
CLEANUPDEPS = $(wildcard cleanup/*/*)
CS = $(subst console-setup/dependencies.d,, $(wildcard console-setup/*))
CSDEPS = $(wildcard console-setup/*/*)
DEFAULT = $(subst default/contents.d,, $(wildcard default/*))
DEFAULTCONTENTS = $(wildcard default/*/*)
DMESGLOG = $(wildcard dmesg-log/*)
DMESGSRV = $(subst dmesg-srv/dependencies.d,, $(wildcard dmesg-srv/*))
DMESGSRVDEPS = $(wildcard dmesg-srv/*/*)
GETTY = $(subst getty/contents.d,, $(wildcard getty/*))
GETTYCONTENTS = $(wildcard getty/*/*)
HOSTNAME = $(subst hostname/dependencies.d,, $(wildcard hostname/*))
HOSTNAMEDEPS = $(wildcard hostname/*/*)
HWCLOCK = $(wildcard hwclock/*)
LOCALE = $(subst locale/dependencies.d,, $(wildcard locale/*))
LOCALEDEPS = $(wildcard locale/*/*)
MISC = $(subst misc/contents.d,, $(wildcard misc/*))
MISCCONTENTS = $(wildcard misc/*/*)
MODULES = $(subst modules/dependencies.d,, $(wildcard modules/*))
MODULESDEPS = $(wildcard modules/*/*)
MOUNT = $(subst mount/contents.d,, $(wildcard mount/*))
MOUNTCONTENTS = $(wildcard mount/*/*)
MCG = $(subst mount-cgroups/dependencies.d,, $(wildcard mount-cgroups/*))
MCGDEPS = $(wildcard mount-cgroups/*/*)
MDEVFS = $(subst mount-devfs/dependencies.d,, $(wildcard mount-devfs/*))
MDEVFSDEPS = $(wildcard mount-devfs/*/*)
MFS = $(subst mount-filesystems/dependencies.d,, $(wildcard mount-filesystems/*))
MFSDEPS = $(wildcard mount-filesystems/*/*)
MNET = $(subst mount-net/dependencies.d,, $(wildcard mount-net/*))
MNETDEPS = $(wildcard mount-net/*/*)
MPROC = $(wildcard mount-procfs/*)
MSYSFS = $(subst mount-sysfs/dependencies.d,, $(wildcard mount-sysfs/*))
MSYSFSDEPS = $(wildcard mount-sysfs/*/*)
MTMPFS = $(wildcard mount-tmpfs/*)
NETLO = $(wildcard net-lo/*)
NETWO = $(subst network/contents.d,, $(wildcard network/*))
NETWOCONTENTS = $(wildcard network/*/*)
NETWODETECT = $(subst network-detection/dependencies.d,, $(wildcard network-detection/*))
NETWODETECTDEPS = $(wildcard network-detection/*/*)
RNDSEED = $(subst random-seed/dependencies.d,, $(wildcard random-seed/*))
RNDSEEDDEPS = $(wildcard random-seed/*/*)
RCLOCAL = $(subst rc-local/dependencies.d,, $(wildcard rc-local/*))
RCLOCALDEPS = $(wildcard rc-local/*/*)
REMNTROOT = $(subst remount-root/dependencies.d,, $(wildcard remount-root/*))
REMNTROOTDEPS = $(wildcard remount-root/*/*)
SETUP = $(subst setup/contents.d,, $(wildcard setup/*))
SETUPCONTENTS = $(wildcard setup/*/*)
SWAP = $(subst swap/dependencies.d,, $(wildcard swap/*))
SWAPDEPS = $(wildcard swap/*/*)
SYSCTL = $(subst sysctl/dependencies.d,, $(wildcard sysctl/*))
SYSCTLDEPS = $(wildcard sysctl/*/*)
TTY1 = $(subst tty1/dependencies.d,, $(wildcard tty1/*))
TTY1DEPS = $(wildcard tty1/*/*)
TTY2 = $(subst tty2/dependencies.d,, $(wildcard tty2/*))
TTY2DEPS = $(wildcard tty2/*/*)
TTY3 = $(subst tty3/dependencies.d,, $(wildcard tty3/*))
TTY3DEPS = $(wildcard tty3/*/*)
TTY4 = $(subst tty4/dependencies.d,, $(wildcard tty4/*))
TTY4DEPS = $(wildcard tty4/*/*)
TTY5 = $(subst tty5/dependencies.d,, $(wildcard tty5/*))
TTY5DEPS = $(wildcard tty5/*/*)
TTY6 = $(subst tty6/dependencies.d,, $(wildcard tty6/*))
TTY6DEPS = $(wildcard tty6/*/*)
TTYS = $(subst ttyS/dependencies.d,, $(wildcard ttyS/*))
TTYSDEPS = $(wildcard ttyS/*/*)
UDEV = $(subst udev/contents.d,, $(wildcard udev/*))
UDEVCONTENTS = $(wildcard udev/*/*)
UDEVADM = $(subst udevadm/dependencies.d,, $(wildcard udevadm/*))
UDEVADMDEPS = $(wildcard udevadm/*/*)
UDEVLOG = $(wildcard udevd-log/*)
UDEVSRV = $(subst udevd-srv/dependencies.d,, $(wildcard udevd-srv/*))
UDEVSRVDEPS = $(wildcard udevd-srv/*/*)

DIRMODE = -dm0755
MODE = -m0644
EMODE = -m0755

install_s6:
	install $(DIRMODE) $(DESTDIR)$(SVDIR)
	install $(DIRMODE) $(DESTDIR)$(CONFIGDIR)

	install $(MODE) $(CONFIG) $(DESTDIR)$(CONFIGDIR)

	install $(MODE) $(RC) $(DESTDIR)$(S6DIR)

	install $(MODE) $(LOGRELOAD) $(DESTDIR)$(S6DIR)

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(BOOTDIR)/contents.d
	install $(MODE) $(BOOT) $(DESTDIR)$(SVDIR)/$(BOOTDIR)
	install $(MODE) $(BOOTCONTENTS) $(DESTDIR)$(SVDIR)/$(BOOTDIR)/contents.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(CLEANUPDIR)/dependencies.d
	install $(MODE) $(CLEANUP) $(DESTDIR)$(SVDIR)/$(CLEANUPDIR)
	install $(MODE) $(CLEANUPDEPS) $(DESTDIR)$(SVDIR)/$(CLEANUPDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(CSDIR)/dependencies.d
	install $(MODE) $(CS) $(DESTDIR)$(SVDIR)/$(CSDIR)
	install $(MODE) $(CSDEPS) $(DESTDIR)$(SVDIR)/$(CSDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(ADMINSVDIR)/$(DEFAULTDIR)/contents.d
	install $(MODE) $(DEFAULT) $(DESTDIR)$(ADMINSVDIR)/$(DEFAULTDIR)
	install $(MODE) $(DEFAULTCONTENTS) $(DESTDIR)$(ADMINSVDIR)/$(DEFAULTDIR)/contents.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(DMESGLOGDIR)
	install $(MODE) $(DMESGLOG) $(DESTDIR)$(SVDIR)/$(DMESGLOGDIR)

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(DMESGSRVDIR)/dependencies.d
	install $(MODE) $(DMESGSRV) $(DESTDIR)$(SVDIR)/$(DMESGSRVDIR)
	install $(MODE) $(DMESGSRVDEPS) $(DESTDIR)$(SVDIR)/$(DMESGSRVDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(GETTYDIR)/contents.d
	install $(MODE) $(GETTY) $(DESTDIR)$(SVDIR)/$(GETTYDIR)
	install $(MODE) $(GETTYCONTENTS) $(DESTDIR)$(SVDIR)/$(GETTYDIR)/contents.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(HOSTNAMEDIR)/dependencies.d
	install $(MODE) $(HOSTNAME) $(DESTDIR)$(SVDIR)/$(HOSTNAMEDIR)
	install $(MODE) $(HOSTNAMEDEPS) $(DESTDIR)$(SVDIR)/$(HOSTNAMEDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(HWCLOCKDIR)
	install $(MODE) $(HWCLOCK) $(DESTDIR)$(SVDIR)/$(HWCLOCKDIR)

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(LOCALEDIR)/dependencies.d
	install $(MODE) $(LOCALE) $(DESTDIR)$(SVDIR)/$(LOCALEDIR)
	install $(MODE) $(LOCALEDEPS) $(DESTDIR)$(SVDIR)/$(LOCALEDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(MISCDIR)/contents.d
	install $(MODE) $(MISC) $(DESTDIR)$(SVDIR)/$(MISCDIR)
	install $(MODE) $(MISCCONTENTS) $(DESTDIR)$(SVDIR)/$(MISCDIR)/contents.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(MODULESDIR)/dependencies.d
	install $(MODE) $(MODULES) $(DESTDIR)$(SVDIR)/$(MODULESDIR)
	install $(MODE) $(MODULESDEPS) $(DESTDIR)$(SVDIR)/$(MODULESDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(PREFIX)/bin
	install $(EMODE) $(MODLOAD) $(DESTDIR)$(PREFIX)/bin

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(MOUNTDIR)/contents.d
	install $(MODE) $(MOUNT) $(DESTDIR)$(SVDIR)/$(MOUNTDIR)
	install $(MODE) $(MOUNTCONTENTS) $(DESTDIR)$(SVDIR)/$(MOUNTDIR)/contents.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(MCGDIR)/dependencies.d
	install $(MODE) $(MCG) $(DESTDIR)$(SVDIR)/$(MCGDIR)
	install $(MODE) $(MCGDEPS) $(DESTDIR)$(SVDIR)/$(MCGDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(MDEVFSDIR)/dependencies.d
	install $(MODE) $(MDEVFS) $(DESTDIR)$(SVDIR)/$(MDEVFSDIR)
	install $(MODE) $(MDEVFSDEPS) $(DESTDIR)$(SVDIR)/$(MDEVFSDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(MFSDIR)/dependencies.d
		install $(MODE) $(MFS) $(DESTDIR)$(SVDIR)/$(MFSDIR)
	install $(MODE) $(MFSDEPS) $(DESTDIR)$(SVDIR)/$(MFSDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(MNETDIR)/dependencies.d
	install $(MODE) $(MNET) $(DESTDIR)$(SVDIR)/$(MNETDIR)
	install $(MODE) $(MNETDEPS) $(DESTDIR)$(SVDIR)/$(MNETDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(MPROCDIR)
	install $(MODE) $(MPROC) $(DESTDIR)$(SVDIR)/$(MPROCDIR)

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(MSYSFSDIR)/dependencies.d
	install $(MODE) $(MSYSFS) $(DESTDIR)$(SVDIR)/$(MSYSFSDIR)
	install $(MODE) $(MSYSFSDEPS) $(DESTDIR)$(SVDIR)/$(MSYSFSDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(MTMPFSDIR)
	install $(MODE) $(MTMPFS) $(DESTDIR)$(SVDIR)/$(MTMPFSDIR)

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(NETLODIR)
	install $(MODE) $(NETLO) $(DESTDIR)$(SVDIR)/$(NETLODIR)

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(NETWODIR)/contents.d
	install $(MODE) $(NETWO) $(DESTDIR)$(SVDIR)/$(NETWODIR)
	install $(MODE) $(NETWOCONTENTS) $(DESTDIR)$(SVDIR)/$(NETWODIR)/contents.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(NETWODETECTDIR)/dependencies.d
	install $(MODE) $(NETWODETECT) $(DESTDIR)$(SVDIR)/$(NETWODETECTDIR)
	install $(MODE) $(NETWODETECTDEPS) $(DESTDIR)$(SVDIR)/$(NETWODETECTDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(RNDSEEDDIR)/dependencies.d
	install $(MODE) $(RNDSEED) $(DESTDIR)$(SVDIR)/$(RNDSEEDDIR)
	install $(MODE) $(RNDSEEDDEPS) $(DESTDIR)$(SVDIR)/$(RNDSEEDDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(RCLOCALDIR)/dependencies.d
	install $(MODE) $(RCLOCAL) $(DESTDIR)$(SVDIR)/$(RCLOCALDIR)
	install $(MODE) $(RCLOCALDEPS) $(DESTDIR)$(SVDIR)/$(RCLOCALDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(REMNTROOTDIR)/dependencies.d
	install $(MODE) $(REMNTROOT) $(DESTDIR)$(SVDIR)/$(REMNTROOTDIR)
	install $(MODE) $(REMNTROOTDEPS) $(DESTDIR)$(SVDIR)/$(REMNTROOTDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(SETUPDIR)/contents.d
	install $(MODE) $(SETUP) $(DESTDIR)$(SVDIR)/$(SETUPDIR)
	install $(MODE) $(SETUPCONTENTS) $(DESTDIR)$(SVDIR)/$(SETUPDIR)/contents.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(SWAPDIR)/dependencies.d
	install $(MODE) $(SWAP) $(DESTDIR)$(SVDIR)/$(SWAPDIR)
	install $(MODE) $(SWAPDEPS) $(DESTDIR)$(SVDIR)/$(SWAPDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(SYSCTLDIR)/dependencies.d
	install $(MODE) $(SYSCTL) $(DESTDIR)$(SVDIR)/$(SYSCTLDIR)
	install $(MODE) $(SYSCTLDEPS) $(DESTDIR)$(SVDIR)/$(SYSCTLDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(TTY1DIR)/dependencies.d
	install $(MODE) $(TTY1) $(DESTDIR)$(SVDIR)/$(TTY1DIR)
	install $(MODE) $(TTY1DEPS) $(DESTDIR)$(SVDIR)/$(TTY1DIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(TTY2DIR)/dependencies.d
	install $(MODE) $(TTY2) $(DESTDIR)$(SVDIR)/$(TTY2DIR)
	install $(MODE) $(TTY2DEPS) $(DESTDIR)$(SVDIR)/$(TTY2DIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(TTY3DIR)/dependencies.d
	install $(MODE) $(TTY3) $(DESTDIR)$(SVDIR)/$(TTY3DIR)
	install $(MODE) $(TTY3DEPS) $(DESTDIR)$(SVDIR)/$(TTY3DIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(TTY4DIR)/dependencies.d
	install $(MODE) $(TTY4) $(DESTDIR)$(SVDIR)/$(TTY4DIR)
	install $(MODE) $(TTY4DEPS) $(DESTDIR)$(SVDIR)/$(TTY4DIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(TTY5DIR)/dependencies.d
	install $(MODE) $(TTY5) $(DESTDIR)$(SVDIR)/$(TTY5DIR)
	install $(MODE) $(TTY5DEPS) $(DESTDIR)$(SVDIR)/$(TTY5DIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(TTY6DIR)/dependencies.d
	install $(MODE) $(TTY6) $(DESTDIR)$(SVDIR)/$(TTY6DIR)
	install $(MODE) $(TTY6DEPS) $(DESTDIR)$(SVDIR)/$(TTY6DIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(TTYSDIR)/dependencies.d
	install $(MODE) $(TTYS) $(DESTDIR)$(SVDIR)/$(TTYSDIR)
	install $(MODE) $(TTYSDEPS) $(DESTDIR)$(SVDIR)/$(TTYSDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(UDEVDIR)/contents.d
	install $(MODE) $(UDEV) $(DESTDIR)$(SVDIR)/$(UDEVDIR)
	install $(MODE) $(UDEVCONTENTS) $(DESTDIR)$(SVDIR)/$(UDEVDIR)/contents.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(UDEVADMDIR)/dependencies.d
	install $(MODE) $(UDEVADM) $(DESTDIR)$(SVDIR)/$(UDEVADMDIR)
	install $(MODE) $(UDEVADMDEPS) $(DESTDIR)$(SVDIR)/$(UDEVADMDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(UDEVLOGDIR)
	install $(MODE) $(UDEVLOG) $(DESTDIR)$(SVDIR)/$(UDEVLOGDIR)

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(UDEVSRVDIR)/dependencies.d
	install $(MODE) $(UDEVSRV) $(DESTDIR)$(SVDIR)/$(UDEVSRVDIR)
	install $(MODE) $(UDEVSRVDEPS) $(DESTDIR)$(SVDIR)/$(UDEVSRVDIR)/dependencies.d

install: install_s6

